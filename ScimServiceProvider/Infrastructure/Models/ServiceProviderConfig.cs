﻿using ScimServiceProvider.Infrastructure.Models.AuxModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ScimServiceProvider.Infrastructure.Models
{
	public class ServiceProviderConfig
	{
        [JsonPropertyName("schemas")]
        public List<string> Schemas { get; set; }

        [JsonPropertyName("documentationUri")]
        public string DocumentationUri { get; set; }

        [JsonPropertyName("patch")]
        public Patch Patch { get; set; }

        [JsonPropertyName("bulk")]
        public Bulk Bulk { get; set; }

        [JsonPropertyName("filter")]
        public Filter Filter { get; set; }

        [JsonPropertyName("changePassword")]
        public ChangePassword ChangePassword { get; set; }

        [JsonPropertyName("sort")]
        public Sort Sort { get; set; }

        [JsonPropertyName("etag")]
        public Etag Etag { get; set; }

        [JsonPropertyName("authenticationSchemes")]
        public List<AuthenticationScheme> AuthenticationSchemes { get; set; }

        [JsonPropertyName("meta")]
        public Meta Meta { get; set; }
    }

    public class Patch
    {
        [JsonPropertyName("supported")]
        public bool Supported { get; set; }
    }

    public class Bulk
    {
        [JsonPropertyName("supported")]
        public bool Supported { get; set; }

        [JsonPropertyName("maxOperations")]
        public int MaxOperations { get; set; }

        [JsonPropertyName("maxPayloadSize")]
        public int MaxPayloadSize { get; set; }
    }

    public class Filter
    {
        [JsonPropertyName("supported")]
        public bool Supported { get; set; }

        [JsonPropertyName("maxResults")]
        public int MaxResults { get; set; }
    }

    public class ChangePassword
    {
        [JsonPropertyName("supported")]
        public bool Supported { get; set; }
    }

    public class Sort
    {
        [JsonPropertyName("supported")]
        public bool Supported { get; set; }
    }

    public class Etag
    {
        [JsonPropertyName("supported")]
        public bool Supported { get; set; }
    }

    public class AuthenticationScheme
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("description")]
        public string Description { get; set; }

        [JsonPropertyName("specUri")]
        public string SpecUri { get; set; }

        [JsonPropertyName("documentationUri")]
        public string DocumentationUri { get; set; }

        [JsonPropertyName("type")]
        public string Type { get; set; }

        [JsonPropertyName("primary")]
        public bool Primary { get; set; }
    }
}
