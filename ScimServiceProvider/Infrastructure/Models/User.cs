﻿using ScimServiceProvider.Infrastructure.Dynamic;
using ScimServiceProvider.Infrastructure.Models.AuxModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ScimServiceProvider.Infrastructure.Models
{
    [GeneratedController("scim/Iñi")]
    public class Iñi: Resource
	{
        public bool Activa { get; set; }
	}

    [GeneratedController("scim/User")]
    public class User: Resource
	{

        [JsonPropertyName("userName")]
        public string UserName { get; set; }

        [JsonPropertyName("name")]
        public Name Name { get; set; }

        [JsonPropertyName("displayName")]
        public string DisplayName { get; set; }

        [JsonPropertyName("nickName")]
        public string NickName { get; set; }

        [JsonPropertyName("profileUrl")]
        public string ProfileUrl { get; set; }

        [JsonPropertyName("emails")]
        public List<Email> Emails { get; set; }

        [JsonPropertyName("addresses")]
        public List<Address> Addresses { get; set; }

        [JsonPropertyName("phoneNumbers")]
        public List<PhoneNumber> PhoneNumbers { get; set; }

        [JsonPropertyName("ims")]
        public List<Im> Ims { get; set; }

        [JsonPropertyName("photos")]
        public List<Photo> Photos { get; set; }

        [JsonPropertyName("userType")]
        public string UserType { get; set; }

        [JsonPropertyName("title")]
        public string Title { get; set; }

        [JsonPropertyName("preferredLanguage")]
        public string PreferredLanguage { get; set; }

        [JsonPropertyName("locale")]
        public string Locale { get; set; }

        [JsonPropertyName("timezone")]
        public string Timezone { get; set; }

        [JsonPropertyName("active")]
        public bool Active { get; set; }

        [JsonPropertyName("password")]
        public string Password { get; set; }

        [JsonPropertyName("groups")]
        public List<Group> Groups { get; set; }

        [JsonPropertyName("x509Certificates")]
        public List<X509Certificates> X509Certificates { get; set; }


    }
}
