﻿using ScimServiceProvider.Infrastructure.Models.AuxModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ScimServiceProvider.Infrastructure.Models
{
	public abstract class Resource
	{
		[JsonPropertyName("id")]
		public string Id { get; set; }

		[JsonPropertyName("externalId")]
		public string ExternalId { get; set; }

		[JsonPropertyName("meta")]
		public Meta Meta { get; set; }

		[JsonPropertyName("schemas")]
		public List<string> Schemas 
		{	
			get
			{
				return new List<string>() { "urn:ietf:params:scim:schemas:core:2.0:"+this.GetType().Name };
			}
		}
	}
}
