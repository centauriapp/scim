﻿using ScimServiceProvider.Infrastructure.Models.AuxModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ScimServiceProvider.Infrastructure.Models
{
	public class ResourceType
	{
        [JsonPropertyName("schemas")]
        public List<string> Schemas { get; set; }

        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("endpoint")]
        public string Endpoint { get; set; }

        [JsonPropertyName("description")]
        public string Description { get; set; }

        [JsonPropertyName("schema")]
        public string Schema { get; set; }

        [JsonPropertyName("schemaExtensions")]
        public List<SchemaExtension> SchemaExtensions { get; set; }

        [JsonPropertyName("meta")]
        public Meta Meta { get; set; }
    }

    public class SchemaExtension
    {
        [JsonPropertyName("schema")]
        public string Schema { get; set; }

        [JsonPropertyName("required")]
        public bool Required { get; set; }
    }
}
