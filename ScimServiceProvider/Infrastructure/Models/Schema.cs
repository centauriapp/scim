﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ScimServiceProvider.Infrastructure.Models
{
	public class Schema
	{
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("description")]
        public string Description { get; set; }

        [JsonPropertyName("attributes")]
        public List<Attribute> Attributes { get; set; }
    }

    public class Attribute
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("type")]
        public string Type { get; set; }

        [JsonPropertyName("multiValued")]
        public bool MultiValued { get; set; }

        [JsonPropertyName("description")]
        public string Description { get; set; }

        [JsonPropertyName("required")]
        public bool Required { get; set; }

        [JsonPropertyName("caseExact")]
        public bool CaseExact { get; set; }

        [JsonPropertyName("mutability")]
        public string Mutability { get; set; }

        [JsonPropertyName("returned")]
        public string Returned { get; set; }

        [JsonPropertyName("uniqueness")]
        public string Uniqueness { get; set; }

        [JsonPropertyName("subAttributes")]
        public List<Attribute> SubAttributes { get; set; }
    }
}
