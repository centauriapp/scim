﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ScimServiceProvider.Infrastructure.Models.AuxModels
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Name
    {
        [JsonPropertyName("formatted")]
        public string Formatted { get; set; }

        [JsonPropertyName("familyName")]
        public string FamilyName { get; set; }

        [JsonPropertyName("givenName")]
        public string GivenName { get; set; }

        [JsonPropertyName("middleName")]
        public string MiddleName { get; set; }

        [JsonPropertyName("honorificPrefix")]
        public string HonorificPrefix { get; set; }

        [JsonPropertyName("honorificSuffix")]
        public string HonorificSuffix { get; set; }
    }

    public class Email
    {
        [JsonPropertyName("value")]
        public string Value { get; set; }

        [JsonPropertyName("type")]
        public string Type { get; set; }

        [JsonPropertyName("primary")]
        public bool Primary { get; set; }
    }

    public class Address
    {
        [JsonPropertyName("type")]
        public string Type { get; set; }

        [JsonPropertyName("streetAddress")]
        public string StreetAddress { get; set; }

        [JsonPropertyName("locality")]
        public string Locality { get; set; }

        [JsonPropertyName("region")]
        public string Region { get; set; }

        [JsonPropertyName("postalCode")]
        public string PostalCode { get; set; }

        [JsonPropertyName("country")]
        public string Country { get; set; }

        [JsonPropertyName("formatted")]
        public string Formatted { get; set; }

        [JsonPropertyName("primary")]
        public bool Primary { get; set; }
    }

    public class PhoneNumber
    {
        [JsonPropertyName("value")]
        public string Value { get; set; }

        [JsonPropertyName("type")]
        public string Type { get; set; }
    }

    public class Im
    {
        [JsonPropertyName("value")]
        public string Value { get; set; }

        [JsonPropertyName("type")]
        public string Type { get; set; }
    }

    public class Photo
    {
        [JsonPropertyName("value")]
        public string Value { get; set; }

        [JsonPropertyName("type")]
        public string Type { get; set; }
    }

    public class Group
    {
        [JsonPropertyName("value")]
        public string Value { get; set; }

        [JsonPropertyName("$ref")]
        public string Ref { get; set; }

        [JsonPropertyName("display")]
        public string Display { get; set; }
    }

    public class X509Certificates
    {
        [JsonPropertyName("value")]
        public string Value { get; set; }
    }

    public class Meta
    {
        [JsonPropertyName("resourceType")]
        public string ResourceType { get; set; }

        [JsonPropertyName("created")]
        public DateTime Created { get; set; }

        [JsonPropertyName("lastModified")]
        public DateTime LastModified { get; set; }

        [JsonPropertyName("version")]
        public string Version { get; set; }

        [JsonPropertyName("location")]
        public string Location { get; set; }
    }

}
