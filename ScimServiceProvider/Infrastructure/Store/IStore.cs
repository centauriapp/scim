﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScimServiceProvider.Infrastructure.Interfaces
{
	public interface IStore<T>
	{
		T GetById(string id);
		T Add(T resource);
		T Replace(string id, T resource);
		T Update(string id, T resource);
		T Search(Predicate<T> query);
	}
}
