﻿using ScimServiceProvider.Infrastructure.Interfaces;
using ScimServiceProvider.Infrastructure.Models;
using System;

namespace ScimServiceProvider.Infrastructure.Store
{
	public class UserStore : IStore<User>
	{
		public User Add(User resource)
		{
			return new User();
			//throw new NotImplementedException();
		}

		public User GetById(string id)
		{
			throw new NotImplementedException();
		}

		public User Replace(string id, User resource)
		{
			throw new NotImplementedException();
		}

		public User Search(Predicate<User> query)
		{
			throw new NotImplementedException();
		}

		public User Update(string id, User resource)
		{
			throw new NotImplementedException();
		}
	}
}
