﻿using Microsoft.AspNetCore.Mvc;
using ScimServiceProvider.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using DynamicExpression = System.Linq.Dynamic.Core.DynamicExpressionParser; //nuget package


namespace ScimServiceProvider.Controllers
{
	public class BaseController<T>: Controller where T : class
	{
        private readonly IStore<T> _storage;

        public BaseController(IStore<T> storage)
        {
            _storage = storage;
        }

        [HttpGet("{id}")]
        public T Get(string id)
        {
            return _storage.GetById(id);
        }

        [HttpPost]
        public T Post([FromBody] T value)
        {
            return _storage.Add(value);
        }

        [HttpPut("{id}")]
        public T Put(string id, [FromBody] T value)
        {
            return _storage.Replace(id, value);
        }

        [HttpPatch("{id}")]
        public T Patch(string id, [FromBody] T value)
        {
            return _storage.Update(id, value);
        }

        [HttpPost(".search")]
        public void Search(string query)
        {
            var config = new ParsingConfig();
            var list = new List<string>();
            var exp = DynamicExpression.ParseLambda<T, bool>(config, true, ToStringExpression(query)).Compile();
            var predicate = new Predicate<T>(exp);
            _storage.Search(predicate);
        }

        private string ToStringExpression(string scimExpression)
		{
            return scimExpression
                .Replace(" eq ", " = ")
                .Replace(" ne ", " != ")
                .Replace(" gt ", " > ")
                .Replace(" ge ", " >= ")
                .Replace(" lt ", " < ")
                .Replace(" le ", " <= ");

        }
    }
}
