﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ScimServiceProvider.Infrastructure.Models;
using ScimServiceProvider.Infrastructure.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScimServiceProvider.Controllers
{
	[Route("scim/[controller]")]
	[ApiController]
	public class UserController : BaseController<User>
	{
		public UserController(UserStore store): base(store)
		{

		}
	}
}
